# conventions 

## Modules 

modules are logical "steps" of a workflow, and go into the workflows/modules group 

main branches are the default tools, for special cases we use branches

## Workflows

### Config

config files are project specific 

naming conventions: 
  - parameters of a rule should have the same name in the config file
  - we use snake_case and not camelCase.

### Rules

rule should be name with do_ prefix if the cannot be executed standalone.

### Versioning

For tag versioning follow [sematic versioning schema](https://semver.org/)




